<?php

class WhiteRabbit
{
       
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //TODO implement this!
        $arr1 = array();
        $data = file_get_contents($filePath);
        

    if (empty($data) != true) 
    {
        
        $keys = range('a','z');
        $values = array_fill(0,26,0);
        $occurrences = array_combine($keys,$values);
        $length = strlen($data);
        
        for($i = 0; $i<$length; $i++){
            $letter = strtolower($data[$i]);
        if(array_key_exists($letter,$occurrences)){
            $occurrences{$letter}++;
        }
        }

        return $occurrences;
    }
    else throw new Exception('File path is not correct.');
    }
    



    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
       //TODO implement this!
        arsort($parsedFile);
        $keysAmount = count($parsedFile);
        $median = $keysAmount/2;
        $letters = array();
        
           foreach($parsedFile as $i => $value){
            
            array_push($letters,array ("letter" => $i, "count" => $value));
        
           }
      
           $occurrences=$letters[$median-1]["count"];
           return $letters[$median-1]["letter"];       
    }  

    
}
$klasa = new WhiteRabbit();
$klasa -> findMedianLetterInFile(__DIR__ ."/../txt/text3.txt");

?>