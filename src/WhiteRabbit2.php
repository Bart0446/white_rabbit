<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
       
        $a=0;
        $b=0;
        $c=0;
        $d=0;
        $e=0;
        $f=0;
        $g=0;  
    
        
        while($amount > 0)
        {
            if($amount >=100)
            {
                $g++;
                $amount=$amount-100;
            }              
            if($amount<100 && $amount >=50)
            {
                $f++;
                $amount-=50;
            }              
            if($amount<50 && $amount >=20)
            {
                $e++;
                $amount-=20;
            }                
            if($amount<20 && $amount >=10)
            {
                $d++;
                $amount-=10;
            }              
            if($amount<10 && $amount >=5)
            {
                $c++;
                $amount-=5;
            }              
            if($amount<5 && $amount >=2)
            {
                $b++;
                $amount-=2;
            }               
            if($amount<2 && $amount >=1)
            {
                $a++;
                $amount-=1;
            }
               

        }
        
        print_r(array(
            '1'   => $a,
            '2'   => $b,
            '5'   => $c,
            '10'  => $d,
            '20'  => $e,
            '50'  => $f,
            '100' => $g));
      
      
        return array(
                '1'   => $a,
                '2'   => $b,
                '5'   => $c,
                '10'  => $d,
                '20'  => $e,
                '50'  => $f,
                '100' => $g
            );
        
       
    }
}


?>